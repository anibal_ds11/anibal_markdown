# HOJA03_Markdown_02  

#### a)Creamos un repositorio  
![](Captura.PNG "Captura1")  
#### b)Clonamos el repositorio   
Para clonar hacemos un clone del repositorio con la orden "git clone gitlab/respoitorio"      
![](Captura2.PNG "Captura2")  
#### d)Y ahora creamos el documento y hacemos el commit. Podemos crear el documento con el touch.   
~~~
git init
gitt add
git commit -m
~~~

![](Captura3.PNG "Captura3")  
#### e)Los añadimos y hacemos un push.    

~~~
git remote add origin
git push origin master
~~~
![](Captura4.PNG "Captura4")  
#### f)Creamos los ficheros indicados. 

~~~
touch 
nano
~~~  
![](Captura5.PNG "Captura5")  
#### g)Para ignorar archivos debemos crear un archivo .gitignore, y escribir los ficheros que queremos ignorar.  
![](Captura6.PNG "Captura6")  
#### i) Añadimos la carpeta con los modulos.  
![](Captura7.PNG "Captura7")  
#### j) Creamos el tag llamado "v0.1".  
![](Captura8.PNG "Captura8")  
#### k) Hacemos el add, el commit y el push al directorio.  
![](Captura9.PNG "Captura9")    

Curso | Alumno
--|--
DAW|Aníbal
DAW|Candido

https://gitlab.com/anibal_ds11/anibal_markdown.git  


# HOJA03_Markdown_03  

#### 1.a) Creamos la rama y directamente nos posicionamos en ella:

~~~
git checkout -b Anibal
~~~  

![](Hoja03/Captura01.PNG "Captura10")   
#### 1.b) Con el checkout creamos la rama y directamente nos posicionamos en esta.

#### 2.c) Creamos el archivo y lo modificamos en la rama en la que estamos
~~~
touch daw.md
nano daw.md
~~~
![](Hoja03/Captura06.PNG "Captura11")   

#### 2.d) Hacemos un add y despues el commit con el archivo.

~~~
git add .
git commit -m "Añadiendo el archivo daw.md a la rama Anibal"
~~~
![](Hoja03/Captura02.PNG "Captura12")   

#### 2.e) Hacemos el push con la rama creada anteriormente.

~~~
git push origin master
~~~
![](Hoja03/Captura03.PNG "Captura13")   

#### 3.f) Ahora nos movemos a la rama master.

~~~
git checkout master
~~~
![](Hoja03/Captura04.PNG "Captura14") 

#### 3.g) Hacemos el merge para unir ambas ramas.

~~~
git merge rama-Anibal
~~~
![](Hoja03/Captura07.PNG "Captura15") 

#### 4.h) Usamos nano para modificar el archivo y añadimos la tabla.

![](Hoja03/Captura08.PNG "Captura16") 

#### 4.i) Ahora añadimos los archivos a la rama.

~~~
git add .
git commit -m "Añadida tabla DAW1"
~~~
![](Hoja03/Captura09.PNG "Captura17") 

#### 4.j) Cambiamos ahora de rama de nuevo.

~~~
git checkout rama-Anibal
~~~

#### 4.k) Escribimos otra tabla con los modulos de DAW2

![](Hoja03/Captura010.PNG "Captura18") 

#### 4.l) Añadimos los archivos.

~~~
git add .
git commit -m "Añadiendo el archivo daw.md a la rama Anibal"
~~~

![](Hoja03/Captura013.PNG "Captura19") 

#### 4.m) Ahora nos cambiamos a la rama master y hacemos un merge que nos debería dar error.

~~~
git checkout master
git merge rama-Anibal
~~~

![](Hoja03/Captura014.PNG "Captura20")

#### 5.n) El error esta en el daw.md, y para solucionarlo habria que cambiar el archivo y volver a hacer el merge.


![](Hoja03/Captura011.PNG "Captura21")

##### Solucionamos el error:

![](Hoja03/Captura012.PNG "Captura22")

##### Solucionado el error hacemos un add y commit y comprobamos con un merge que ha funcionado correctamente.

![](Hoja03/Captura015.PNG "Captura23")

#### 6.o)Creamos un tag

~~~
git tag v0.2
~~~

#### 6.p) Y ahora borramos la rama creada anteriormente

~~~
git branch -d master
~~~